package com.refactorizacion.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JobLoggerApplication {

	@Autowired
	private static JobLogger jobLogger;
	
	public static void main(String[] args) {
		SpringApplication.run(JobLoggerApplication.class, args);
		
		try {
			
			Map<String, String> dbParamsMap = new HashMap<String, String>();
			dbParamsMap.put("userName", "lgarciare");
			dbParamsMap.put("password", "12345");
			dbParamsMap.put("dbms", "oracle:thin:@lgarciare:1521:JOB");
			dbParamsMap.put("serverName", "localhost");
			dbParamsMap.put("portNumber", "8090");
			dbParamsMap.put("logFileFolder", "file:///Users/luisgarciareyna/Desktop/⁩");
			
			new JobLogger(true, true, true, true, true, true, dbParamsMap);
			jobLogger.LogMessage("Pruebas JobLogger", true, true, true);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
